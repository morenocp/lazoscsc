#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 13:06:25 2020

@author: pedro
"""
from scipy.signal import butter, lfilter 
import numpy as np 
from HysteresisLoop import HysteresisLoop
from StatusError import StatusError
from pathlib import Path

class ButterBandPassFilter:
    def __init__(self, lowcut, highcut, fs, order):
        nyq = 0.5 * fs 
        low = lowcut/nyq 
        high = highcut/nyq 
        b, a = butter(order, [low, high], btype='band') 
        self.__a=a
        self.__b=b       
    def apply(self,data):
        return lfilter(self.__b, self.__a, data) 

    
class LazosCSC:
    VERT_DIR='v'
    HORIZ_DIR='h'
    def __init__(self):
        self.__data=dict()
        self.__direction=self.HORIZ_DIR
        self.__hysteresis=HysteresisLoop()
        self.__error=StatusError()
        self.__filter=ButterBandPassFilter(lowcut=0.3, highcut=0.7, fs=10, order=2)
        self.__filename=Path()
        
    def setDirectionToAnalyze(self,direction):
        if direction==self.HORIZ_DIR or direction==self.VERT_DIR:
            self.__direction=direction
            
    def __getDirection(self):
        return self.__direction

    def getError(self):
        return self.__error
    
    def getData(self):
        return self.__data
    
    def readFileCSV(self,filename,skipHeader=0):
        result=dict()
        try:
            self.__error.clear()
            data = np.genfromtxt(filename, delimiter=',', skip_header=skipHeader)
            result['t']=data[:,0]
            result['fv']=data[:,2]-data[:,2][0]
            result['fh']=data[:,3]-data[:,3][0]
            result['dv']=data[:,4]-data[:,4][0]
            result['dh']=data[:,5]-data[:,5][0]
            self.__data=dict(result)
            self.__filterData()
            self.__filename=Path(filename)
        except Exception as ex:
            self.__filename=""
            self.__data=dict(result)
            self.__error.setValues(1,"readFileCSV: Error reading file "+str(ex))
        return self.getError()
    
    def __filterData(self):
        dataFiltered=dict()
        for key in self.__data.keys():
            if key !='t':
                dataFiltered[key+'_F']=self.__filter.apply(self.__data[key])
        self.__data.update(dataFiltered)
    
    def getLoopInformation(self,index):
        if self.getError().isOk():
            if index==None:
                self.__error.setValues(0,self.__hysteresis.getLoops())            
            elif self.__hysteresis.getLoopInformation(index).isOk():
                self.__error=self.__hysteresis.getError()
            else:
                self.__error=self.__hysteresis.getError()
        return self.getError()
    
    def getLoopPoints(self,index):
        if self.getError().isOk():
            self.__error=self.__hysteresis.getLoopPoints(index)
        return self.getError()
    
    
    def analyzeLoops(self):
        if self.getError().isOk():
            c=self.__getDirection()
            if self.__hysteresis.setData(self.__data['t'],self.__data['d'+c+'_F'],self.__data['f'+c+'_F']).isOk():
                if self.__hysteresis.analyzeLoops().isOk():
                    self.__error.clear()
            if not self.__hysteresis.getError().isOk():
                self.__error=self.__hysteresis.getError()
        return self.getError()
    
    def exportData(self):
        if self.getError().isOk():
            path=self.__filename
            filenameLoopPoints=path.with_name(path.stem+'_'+self.__getDirection()+'_points'+path.suffix)
            filenameLoopInfo=path.with_name(path.stem+'_'+self.__getDirection()+'_info'+path.suffix)
            if self.__hysteresis.getAllLoopInformation().isOk():
                data=self.__hysteresis.getError().getMessage()
                np.savetxt(filenameLoopInfo, data['data'],header=','.join(data['header']),delimiter=',',fmt='%0.3f')
            else:
                self.__error=self.__hysteresis.getError()
            if self.__hysteresis.getAllLoopPoints().isOk():
                data=self.__hysteresis.getError().getMessage()
                np.savetxt(filenameLoopPoints, data['data'],header=','.join(data['header']),delimiter=',',fmt='%0.3f')
            else:
                self.__error=self.__hysteresis.getError()
        return self.getError()
        
        
#                    
#    
#lazos=LazosCSC()   
#if lazos.readFileCSV('/home/pedro/python_projects/FiltroCSC/datos01.txt').isOk():
#    if lazos.analyzeLoops().isOk():
#        if lazos.getLoopInformation(None).isOk():
#            print(lazos.getError().getMessage())
#            if lazos.getLoopPoints(6).isOk():
#                print(lazos.getError().getMessage())
#if not lazos.getError().isOk():
#    error=lazos.getError()
#    print(error.getCode(),error.getMessage())
#
#
#        
            
        

      
       
        
           
#     
#          
#        
#                
#if __name__ == "__main__": 
#    directorio=pathlib.Path(__file__).parent.absolute()
#    print(directorio)
#    filename=filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
#    print (filename)
#     
#    lazos=LazosCSC()
#    lazos.readFileCSV('/home/pedro/python_projects/FiltroCSC/datos01.txt')
#    lazos.filterData()  
#    lazos.interpOnZero('fh_F')
#    lazos.splitLoops('fh_F')
#    lazos.analyzeAllLoops('fh_F','dh_F')
#    condition=True
#    n=len(lazos.loops)
#    lazo=0
#    
#    while condition:   
#        plt.figure(1)
#        t,f,d=lazos.getLoop(lazo,'fh_F','dh_F')
#        plt.subplot(3,1,1)
#        plt.plot(t,f)
#        plt.subplot(3,1,2)
#        plt.plot(t,d)
#        plt.subplot(3,1,3)
#        plt.plot(d,f)
#        plt.plot(lazos.loops[lazo]['line'][0],lazos.loops[lazo]['line'][1])
#        plt.show()
#        lazo=lazo+1
#        condition=doContinue(1)
#        if lazo==n:
#            condition=False
#
#
##    
    
#    t,f,d=lazos.getLoop(8)
#    print(lazos.analyzeLoop(8))
#
#    
#
#    imaxd=np.where(d==max(d))[0][0]
#    imind=np.where(d==min(d))[0][0]
#    imaxf=np.where(f==max(f))[0][0]
#    iminf=np.where(f==min(f))[0][0]
#    print(imaxf,imaxd)
#    print(iminf,imind)
#
#    p=0.76
#    imin=int((1-p)*iminf+p*imind)
#    imax=int((1-p)*imaxf+p*imaxd)
#    print(imin,imax)
#
#
#    plt.subplot(3,1,1)
#    plt.plot(t,f)
#    plt.subplot(3,1,2)
#    plt.plot(t,d)
#    plt.figure(2)
#
#    plt.plot(d,f)
#    plt.plot([d[imin],d[imax]],[f[imin],f[imax]])
#    plt.show()

 



