#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 16:11:10 2020

@author: pedro
class Grafica

Se utiliza para mostrar en tiempo real el comportamiento de 4 variables
"""
from tkinter import *
from tkinter import Frame

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
#from DequeBuffers import DequeBuffers

class Grafica(Frame):
    FV=0
    FH=1
    DV=2
    DH=3
    def __init__(self,container):
        super().__init__(container)
        self.configure(padx=10,pady=10)
        self.fig = Figure(figsize=(6,4))
        self.axtf = self.fig.add_subplot(3,1,1)            
        self.axtd = self.fig.add_subplot(3,1,2)
        self.axdf = self.fig.add_subplot(3,1,3)                     
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.get_tk_widget().pack(fill="both",expand=True)
        self.canvas.draw() 
    
    def updateFigure(self,data):
        t=data['time']
        f=data['stress']
        d=data['strain']
        line=data['line']
        self.axtf.clear()
        self.axtd.clear()
        self.axdf.clear()
        self.axtf.set_ylabel("Fuerza")  
        self.axtf.set_title("Horizontal") 
        self.axtd.set_ylabel("Desplazamiento") 
        self.axtf.plot(t,f)
        self.axtd.plot(t,d)
        self.axdf.plot(d,f)
        self.axdf.plot(line[0],line[1])
        self.canvas.draw() 

