#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 16:21:17 2020

@author: pedro
class MainFrame
"""

from tkinter import *
from tkinter import ttk

import os

from StatusBar import StatusBar
from StatusError import StatusError
from Grafica import Grafica
from GraficaOriginal import GraficaOriginal 
from LazosCSC import LazosCSC


   

class MainFrame(Tk):
    def __init__(self,container,configuration):
        super().__init__(container)
        self.protocol("WM_DELETE_WINDOW", self.close)
        self.configuration=configuration
        self.commands=self.configuration["CommandList"]
        self.datacfg=self.configuration["AppCfg"]
        self.title(self.datacfg["AppName"])
        self.graph=None #Grafica(None)
        self.__error=StatusError()
        self.initialize()
        self.loops=LazosCSC()
        self.maxLoops=0
        self.loopN=0
        self.mainloop()
          
    def initialize(self):
        #GUI para leer el archivo
        self.filename=StringVar()
        self.strNLoop=StringVar()
        frame1= LabelFrame(self,text="Data filename")
        Label(frame1, text="Filename:").pack(side=LEFT, fill=NONE)
        Entry(frame1,textvariable=self.filename,state='readonly').pack(side=LEFT, fill=X,expand=1)
        Button(frame1,text="Browse...",command=self.openfile).pack(side=LEFT, fill=NONE)
        self.btnProcess=Button(frame1,text="Process data",state=DISABLED,command=self.processData)
        self.btnProcess.pack(side=LEFT, fill=NONE)
        self.btnExport=Button(frame1,text="Export data",state=DISABLED,command=self.exportData)
        self.btnExport.pack(side=RIGHT, fill=NONE)
        frame1.pack(side=TOP, fill=X)
        
       # self.frameLoops=LabelFrame(self,text="Graphics Loop")  
        self.notebook=ttk.Notebook(self)

        page0 = ttk.Frame(self.notebook)
        self.notebook.add(page0, text='Loops')
        self.grafica=Grafica(page0)
        self.grafica.pack(fill=BOTH,expand=1)
        
        page1 = ttk.Frame(self.notebook)
        self.notebook.add(page1, text='Horizontal data')
        self.grafica2=GraficaOriginal(page1)
        self.grafica2.pack(fill=BOTH,expand=1)
        
        page2 = ttk.Frame(self.notebook)
        self.notebook.add(page2, text='Vertical data')
        self.grafica3=GraficaOriginal(page2)
        self.grafica3.pack(fill=BOTH,expand=1)

        
        self.notebook.pack(fill=BOTH,expand=1)
        frameNavigation=Frame(self)       
        Button(frameNavigation,text="|<<",command=self.goFirstLoop).pack(side=LEFT)
        Button(frameNavigation,text="<<",command=self.goPrevLoop).pack(side=LEFT)
       # Entry(frameNavigation,textvariable=self.strNLoop,width=5,justify=CENTER,state="readonly").pack(side=LEFT)
        Button(frameNavigation,text=">>",command=self.goNextLoop).pack(side=LEFT)
        Button(frameNavigation,text=">>|",command=self.goLastLoop).pack(side=LEFT)
        frameNavigation.pack(fill=X)     
         
        #Creacion de la barra de estado
        self.statusBar=StatusBar(self).append("maxloops","Total loops:","0",4)
        self.statusBar.append("loop","Loop:","0",4).append("area","Area: ","0",10)
        self.statusBar.append("t1","T1: ","0",10).append("t2","T2: ","0",10)
        self.statusBar.append("G","G: ","0",10).append("D","D: ","0",10)      
        self.statusBar.append("status","Status: ","Ok")
        self.statusBar.pack(side=BOTTOM,fill=X)

        
    def openfile(self):
        self.statusBar.clear("status")
        currDir = os.getcwd()  
        try:
            filename=filedialog.askopenfilename(initialdir = currDir,title = "Select file",                                           filetypes = (("Text files","*.txt"),("Comma separated values files","*.csv"),("all files","*.*")))
            if len(filename)!=0:
                self.filename.set(filename)
                if self.loops.readFileCSV(filename).isOk():
                    data=self.loops.getData()
                    self.grafica2.updateFigure(data,'h')
                    self.grafica3.updateFigure(data,'v')
                    self.notebook.select(1)
                    self.btnProcess.config(state=NORMAL)
                else:
                    self.__error=self.loops.getError()
                    self.statusBar.setMessage('status','Error: '+self.__error.getMessage())                
            else:
                self.statusBar.setMessage("status","Operation canceled")
        except Exception as ex:
                self.__error.setValues(7,str(ex))
                self.statusBar.setMessage("status","Error: "+str(ex))
                
    def exportData(self):
        if self.loops.exportData().isOk():
            self.statusBar.setMessage("status","Data exported sucessfull")
        else:
            self.__error=self.loops.getError()
            self.statusBar.setMessage("status","Error: Data exported failed")
        
    def processData(self):
        if self.loops.analyzeLoops().isOk():
            if self.loops.getLoopInformation(None).isOk():
                self.loopN=0
                self.maxLoops=len(self.loops.getError().getMessage())
                self.updateGraphLoop()
                self.statusBar.setMessage("maxloops",self.maxLoops)
                self.btnExport.config(state=NORMAL)
                self.statusBar.setMessage('status','Processing data... is Ok')
        else:
            self.__error=self.loops.getError()
            self.statusBar.setMessage('status','Error: '+self.__error.getMessage()) 
        
    
    def goNextLoop(self):
        if self.maxLoops==0:
            return
        self.loopN=self.loopN+1
        if self.loopN < self.maxLoops:
            self.updateGraphLoop()
        else:
            self.loopN=self.loopN-1

    def goLastLoop(self):
        if self.maxLoops==0:
            return
        self.loopN=self.maxLoops-1
        self.updateGraphLoop()
    def goFirstLoop(self):
        if self.maxLoops==0:
            return
        self.loopN=0
        self.updateGraphLoop()
    def goPrevLoop(self):
        if self.maxLoops==0:
            return
        self.loopN=self.loopN-1
        if self.loopN >= 0:
            self.updateGraphLoop()
        else:
            self.loopN=self.loopN+1
    def showLoopInformation(self,loop):
        self.statusBar.setMessage("area",'{:6.2f}'.format(loop['Area']))
        self.statusBar.setMessage('t1','{:6.2f}'.format(loop['aTriang1']))
        self.statusBar.setMessage('t2','{:6.2f}'.format(loop['aTriang2']))
        self.statusBar.setMessage('G','{:6.2f}'.format(loop['G']))
        self.statusBar.setMessage('D','{:2.3f}'.format(loop['D']))
        
    def updateGraphLoop(self):
        n=self.loopN
        if self.loops.getLoopInformation(n).isOk():
            loop=self.loops.getError().getMessage()
            self.statusBar.setMessage("loop",n+1)
            self.showLoopInformation(loop)
            if self.loops.getLoopPoints(n).isOk():
                points=self.loops.getError().getMessage()
                points['line']=loop['line']
                self.grafica.updateFigure(points)


    
    def close(self):
        print("Closing application")
        self.destroy()
       # self.port.closePort();
         