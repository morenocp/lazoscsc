#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 13:06:25 2020

@author: pedro
"""

import numpy as np 
from scipy.interpolate import interp1d
import math
from StatusError import StatusError


class HysteresisLoop:

    def __init__(self,pointsToInterpolate=100):
        self.__data=dict()
        self.__loops=list()
        self.__error=StatusError()
        self.__fInterpStrain=None
        self.__fInterpStress=None
        self.__skipZeroCrossing=3
        self.__pointsToInterpolate=int(pointsToInterpolate/2);
    def getLoops(self):
        return self.__loops
    
    def getData(self):
        return self.__data
        
    def getError(self):
        return self.__error
    
    def setData(self,time,strain,stress,skipZeroCrossing=3):
        lt=len(time)
        ln=len(strain)
        ls=len(stress)
        length=(lt-ln)+(lt-ls)+(ln-ls)
        if length==0:
            result={'time':time,'strain':strain,'stress':stress}
            self.__data.update(result)
        else:
            self.__error.setValues(1,"setData: Unmismatch length of data")
        return self.getError()
    
    def __interpolaLineal(self,x,x1,y1,x2,y2):
        isNew=False
        newY=y1
        if x==x1:
            newY=y1
        elif x==x2:
            newY=y2
        else:
            isNew=True
            newY=(y2-y1)*(x-x1)/(x2-x1)+y1
        return isNew,newY
    
    def __interpOnZero(self):
        if not self.__error.isOk():
            return self.getError()
        self.__error.clear()
        try:
            t=np.array(self.__data['time'])
            x=np.array(self.__data['stress'])
            s=np.array(self.__data['strain'])
            self.__fInterpStrain=interp1d(t,s,'cubic')     
            cruces=np.flip(np.where(np.diff(x>=0)!=0)[0],0)
            for n in cruces:
                isNewt,newt=self.__interpolaLineal(0, x[n],t[n],x[n+1],t[n+1])
                if isNewt:
                    t=np.insert(t,n+1,newt)
                    x=np.insert(x,n+1,0.0)
            self.__data['strain']=self.__fInterpStrain(t)
            self.__data['stress']=x
            self.__data['time']=t
            self.__fInterpStress= interp1d(self.__data['time'],self.__data['stress'],'cubic')
            self.__fInterpStrain= interp1d(self.__data['time'],self.__data['strain'],'cubic')
        except Exception as ex:
            self.__error.setValues(2,'interpOnZero: '+str(ex))
        return self.getError()
        
            
    def __splitLoops(self):
        if not self.__error.isOk():
            return self.__error
        self.__error.clear()
        try:
            zeros=np.where(self.__data['stress']==0)[0]
            initLoop=np.arange(self.__skipZeroCrossing,len(zeros),2)
            for i in initLoop:
                self.__loops.append({'zeroIndex':zeros[i:i+3],'zeroTime':list(self.__data['time'][zeros[i:i+3]])})
            if len(self.__loops[-1]['zeroIndex'])<3:
                self.__loops.pop(-1)
            if len(self.__loops)==0:
                self.__error.setValues(3,'splitLoops: Unavailable loops')
        except Exception as ex:
            self.__error.setValues(3,'splitLoops: '+str(ex))
        return self.getError()
    
    def getLoopPoints(self,index):
        if not self.__error.isOk():
            return self.__error
        self.__error.clear()
        t=list()
        f=list()
        d=list()     
        try:
            if index<=len(self.__loops):
                start=self.__data['time'][self.__loops[index]['zeroIndex'][0]]
                middle=self.__data['time'][self.__loops[index]['zeroIndex'][1]]
                stop=self.__data['time'][self.__loops[index]['zeroIndex'][2]]
                t.extend(np.linspace(start,middle,self.__pointsToInterpolate))
                t.extend(np.linspace(middle,stop,self.__pointsToInterpolate)[1:])
                f=self.__fInterpStress(t)
                d=self.__fInterpStrain(t)
                self.__error.setValues(0,{'time':t,'strain':d,'stress':f})
        except Exception as ex:
            self.__error.setValues(4,'getLoopPoints:['+str(index)+ '] '+str(ex))
        return self.getError()
    
    def getLoopInformation(self,index):
        if not self.__error.isOk():
            return self.__error
        try:
            if index<=len(self.__loops):
                self.__error.setValues(0,self.__loops[index])
            else:
                self.__error.setValues(4,'getLoopInformations: Index loop out of range')
        except Exception as ex:
            self.__error.setValues(4,'getLoopInformations:['+str(index)+ '] '+str(ex))
        return self.getError()
    
        
        
    def __analyzeLoop(self,nLoop):
         result={'Area':0,#'dinit':d[self.l[nLoop]['index'][1]],
                'aTriang1':0,'aTriang2':0,
                'G':0,'D':0,'line':[[0,0],[0,0]]}
         if self.getLoopPoints(nLoop).isOk():
            data=self.__error.getMessage()
            d=data['strain']
            f=data['stress']       
            area=np.trapz(f,d)
            imaxd=np.where(d==max(d))[0][0]
            imind=np.where(d==min(d))[0][0]
            imaxf=np.where(f==max(f))[0][0]
            iminf=np.where(f==min(f))[0][0]
#           p=0.76
#           imin=int((1-p)*iminf+p*imind)
#           imax=int((1-p)*imaxf+p*imaxd)
            imin=imind
            imax=imaxd
            G=(f[imax]-f[imin])/(d[imax]-d[imin])
            d0=d[imax]-(f[imax]/G) #cruce por cero de linea G
            aTriang1=(d[imax]-d0)*f[imax]/2
            aTriang2=(d[imin]-d0)*f[imin]/2
            D=(1/(4*math.pi))*(area/((aTriang1+aTriang2)/2))
            result={'index':nLoop+1,'Area':area,#'dinit':d[self.l[nLoop]['index'][1]],
                'aTriang1':aTriang1,'aTriang2':aTriang2,
                'G':G,'D':D,'line':[[d[imin],d[imax]],[f[imin],f[imax]]]}
         return result         
    
    def analyzeLoops(self):
        if not self.__error.isOk():
            return self.__error
        if not self.__interpOnZero().isOk():
            return self.getError()
        if not self.__splitLoops().isOk():
            return self.getError()
        self.__error.clear()
        for i in range(len(self.__loops)):   
            self.__loops[i].update(self.__analyzeLoop(i))
        return self.getError()
    
    def getAllLoopInformation(self):
        if not self.__error.isOk():
            return self.__error
        try:
            result={'header':['index','Area','aTriang1','aTriang2','G','D'],'data':list()}
            for loop in self.__loops:
                result['data'].append([loop.get(key) for key in result['header']])
            self.__error.setValues(0,result)
        except Exception as ex:
            self.__error.setValues(6,'getAllLoopInformation: '+str(ex))
        return self.getError()
    
    def getAllLoopPoints(self):
        if not self.__error.isOk():
            return self.__error
        try:
            result={'header':list(),'data':list()}
            for i in range(len(self.__loops)):
                strInd='{:03d}'.format(i+1)
                result['header'].extend(['time'+strInd,'strain'+strInd,'stress'+strInd])
                self.getLoopPoints(i)
                points=self.getError().getMessage()
                result['data'].append(points['time'])
                result['data'].append(points['strain'])
                result['data'].append(points['stress'])
            result['data']=np.array(result['data']).transpose()    
            self.__error.setValues(0,result)
        except Exception as ex:
            self.__error.setValues(6,'getAllLoopPoints: '+str(ex))
            
        return self.getError()
            
    
    
            
    
import pandas as pd
data=pd.read_csv("testDataLoop.csv")
t=list(data['time'])
f=list(data['Stress'])
d=list(data['Strain'])

lazos=HysteresisLoop()
if lazos.setData(t,d,f).isOk():
    if lazos.analyzeLoops().isOk():
        if lazos.getAllLoopPoints().isOk():
#        if lazos.getAllLoopInformation().isOk():
            data=lazos.getError().getMessage()
            np.savetxt('array.csv', data['data'],header=','.join(data['header']),delimiter=',',fmt='%0.3f')


if not lazos.getError().isOk():
    print(lazos.getError().getCode(),lazos.getError().getMessage())
