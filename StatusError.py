#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 11:10:24 2020

@author: pedro
"""

class StatusError:
    def __init__(self,code=0,message="",okValue=0):
        self.__okValue=okValue
        self.setValues(code,message)

    def setValues(self, code ,message):
        self.__code=code
        self.__message=message
        return self
    
    def clear(self):
        self.setValues(self.__okValue,"")
    
    def isOk(self):
        return self.__code==self.__okValue
    def getCode(self):
        return self.__code
    def getMessage(self):
        return self.__message




