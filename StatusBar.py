#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 15:50:55 2020

@author: pedro
Class StatusBar
"""
import tkinter as tk


class StatusBar(tk.Frame):   
    def __init__(self, container):
        super().__init__(container)
        self.data={} 
    def __setValue(self,key,value):
        self.data[key]['var'].set(self.data[key]['title']+value)
        
    def append(self, key,title,value,size=None):
        var=tk.StringVar()
        label=tk.Label(self,textvariable=var,justify=tk.LEFT,anchor="w",borderwidth=2, relief="sunken")
        self.data[key]={"var":var,"title":title}
        if size:
            label.config(width=size+len(title))
            label.pack(side=tk.LEFT)
        else:
            label.pack(side=tk.LEFT,fill=tk.X,expand=1)
        self.setMessage(key,value)
        return self
    
    def setMessage(self,key,message):
        if key in self.data.keys():
            self.__setValue(key,str(message))
    def clear(self,key):
        self.setMessage(key,"")
  