function lazos = Ciclo(x,y);
%Aparentemente x =deformacion
%Y=fuerza 
s=size(x);
if s(1,2)>s(1,1) 
    x=x';
end
s=size(y);
if s(1,2)>s(1,1) 
    y=y';
end
j=1;
i=1;
cruces=find(diff(y>=0)~=0);
torig=1:max(size(x));
lazofin=size(cruces)/2;
while j <= lazofin(1,1);
    t=cruces(i):cruces(i)+1;
    t1=interp1(y(t),t,0); %Primer cruce de la fuerza
    i=i+1;
    t=cruces(i):cruces(i)+1;
    t2=interp1(y(t),t,0); %Segundo cruce de la fuerza
    i=i+1;
    t=cruces(i):cruces(i)+1;
    t3=interp1(y(t),t,0); %Tercer cruce de la fuerza
    inc=(t3-t1)/99;
    xt2=interp1(torig,x,t2);  %Deformacion en el segundo cruce
    xint=interp1(torig,x,t1:inc:t3);  %Interpola intervalo de desplazamiento del lazo
    yint=interp1(torig,y,t1:inc:t3);  %Interpola intervalo de fuerza de lazo
    Alazo=trapz(xint,yint);             %Area del lazo
    a=find(yint==max(yint),1,'first'); %indice donde la fuerza es max, Punto 1 de la recta
    b=find(yint==min(yint),1,'first');  %indice donde la fuerza es min Punto 2 de la recta
    G=((yint(a)-yint(b))/(xint(a)-xint(b))); %Pendiente de la recta
    x0=xint(a)-(yint(a)/G);                     %desplazamiento inicial del lazo
    triangulo1=(xint(a)-x0)*yint(a)/2;          %Area del primer triangulo
    triangulo2=(xint(b)-x0)*yint(b)/2;          %Area del segundo triangulo
    D=(1/(4*pi))*(Alazo/((triangulo1+triangulo2)/2)); %Modulo D
    lazos(j,1)=xint(1);
    lazos(j,2)=xt2;
    lazos(j,3)=Alazo;
    lazos(j,4)=triangulo1;
    lazos(j,5)=triangulo2;
    lazos(j,6)=G;
    lazos(j,7)=D;
    j=j+1;
end;