#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 14:23:18 2020

@author: Pedro Moreno
Class para guardar y recuperar datos de configuracion en diccionario a un archivo.
La informacion se guarda en formato JSON
"""
import json

class DataConfig:
    def __init__(self,filename,data=None):
        self._filename=filename
        if (data is None) or not(isinstance(data,dict)):
            self._data=dict()
        else:
            self._data=data
        
    def load(self):
        try:
            with open(self._filename, 'r') as f:
                self._data = json.load(f)
                return True
        except:
            print("Error opening file")
            return False
        
    def save(self):
        try:
            with open(self._filename, 'w') as f:
                json.dump(self._data, f,indent=5)
                return True
        except:
            print("Error opening file")
            return False
            
    def getData(self):
        return self._data
    
 