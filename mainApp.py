# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 09:12:04 2018

@author: pedro
"""
from DataConfig import DataConfig
from MainFrame import MainFrame        

if __name__ == "__main__":
    datacfg=DataConfig("config.cfg")
    if datacfg.load():
        app=MainFrame(None,datacfg.getData())
    else:
        print("Error abriendo archivo de configuracion")
