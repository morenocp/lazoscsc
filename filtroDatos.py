$9s8#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 15:54:47 2020

@author: pedro
"""

from scipy.signal import butter, lfilter 
import numpy as np 
import matplotlib.pyplot as plt 
from scipy.signal import freqz 


def butter_bandpass(lowcut, highcut, fs, order=5): 
    nyq = 0.5 * fs 
    low = lowcut/nyq 
    high = highcut/nyq 
    b, a = butter(order, [low, high], btype='band') 
    return b, a 


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5): 
    b, a = butter_bandpass(lowcut, highcut, fs, order=order) 
    y = lfilter(b, a, data) 
    return y 


if __name__ == "__main__": 


    # Frecuencias de diseñp
    fs = 10 
    lowcut = 0.3 
    highcut = 0.7 

    # Respuesta en freceuencia del filtro  
    plt.figure(1) 
    plt.clf() 
    for order in [4, 6, 8]: #Se prueban filtros con diferente orden
     b, a = butter_bandpass(lowcut, highcut, fs, order=order) 
     w, h = freqz(b, a, worN=2000) 
     plt.plot((fs * 0.5/np.pi) * w, abs(h), label="order = %d" % order) 

    plt.plot([0, 0.5 * fs], [np.sqrt(0.5), np.sqrt(0.5)], 
      '--', label='sqrt(0.5)') 
    plt.xlabel('Frequency (Hz)') 
    plt.ylabel('Gain') 
    plt.grid(True) 
    plt.legend(loc='best') 

    # Filtrado de datos 
    T = 10 
    nsamples = T * fs 
    #np.genfromtxt('datosCSC.csv',delimiter='\t')
    t = np.linspace(0, T, nsamples, endpoint=False) 
    a = 80
    f0 = 0.5
    x = 80* np.sin(2 * np.pi * 10*t) 
    x += 5 * np.cos(2 * np.pi * 5 * t + 0.1) 
    x += a* np.sin(2 * np.pi * f0 * t ) 
    x += 0.03 * np.cos(2 * np.pi * 2000 * t) 
    plt.figure(2) 
    plt.clf() 
    plt.plot(t, x, label='Noisy signal') 
    
    order=4

    y = butter_bandpass_filter(x, lowcut, highcut, fs, order) 
    plt.plot(t, y, label='Filtered signal (%g Hz) with filter order %d' % (f0, order)) 
    plt.xlabel('time (seconds)') 
    plt.hlines([-a, a], 0, T, linestyles='--') 
    plt.grid(True) 
    plt.axis('tight') 
    #plt.legend(loc='upper left') 
    plt.legend(loc='best') 

    plt.show() 