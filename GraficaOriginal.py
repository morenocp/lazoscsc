#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 16:11:10 2020

@author: pedro
class Grafica

Se utiliza para mostrar en tiempo real el comportamiento de 4 variables
"""
from tkinter import *
from tkinter import Frame

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
#from DequeBuffers import DequeBuffers

class GraficaOriginal(Frame):

    def __init__(self,container):
        super().__init__(container)
        self.configure(padx=10,pady=10)
        self.fig = Figure(figsize=(6,4))
        self.axs=list()
        for i in range(6):
            self.axs.append(self.fig.add_subplot(3,2,i+1) )                   
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        self.canvas.get_tk_widget().pack(fill="both",expand=True)
        self.canvas.draw() 
    def __setTitles(self,index,x,y,title):
        self.axs[index].set_xlabel(x) 
        self.axs[index].set_ylabel(y) 
        self.axs[index].set_title(title) 
        
    def __plot(self,index,x,y):
        self.axs[index].plot(x,y)
        
        
    def __updateTitles(self):
        for i in range(6):
            self.axs[i].clear()
        self.__setTitles(0,"","stress","Original data")
        self.__setTitles(1,"","","Filtered data")
        self.__setTitles(2,"","strain","")
        self.__setTitles(4,"strain","stress","")
        self.__setTitles(5,"strain","stress","")
 
    def updateFigure(self,data,key='h'):
        self.__updateTitles()
        t=data['t']
        f=data['f'+key]
        d=data['d'+key]
        ff=data['f'+key+'_F']
        df=data['d'+key+'_F']
        self.__plot(0,t,f)
        self.__plot(1,t,ff)
        self.__plot(2,t,d)
        self.__plot(3,t,df)
        self.__plot(4,d,f)
        self.__plot(5,df,ff)
        self.canvas.draw() 

